
#include <iostream>
#include <time.h>
using namespace std;

int main()
{
	const int  size = 5;
	int array[size][size] = { {0,1,2,3,4}, {2,3,4,5,6}, {4,5,6,7,8}, {6,7,8,9,10}, {11,12,13,14,15} };

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << '\t';
		}
		std::cout << "\n";
	}


	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	std::cout << "current day: " << buf.tm_mday << std::endl;

	int numberStr = buf.tm_mday % size;
	std::cout << "string index: " << numberStr << std::endl;
	int sum = 0;
	
	for (int j = 0; j < size; ++j)
	{
		sum += array[numberStr][j];
	}
	std::cout << "Sum of elements in a row: " << sum << "\n";
	return 0;
	
	
}
